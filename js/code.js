"use strict";
/*________________________________________________________________________________________*/
//one
//
var myFunction = function (message, duration) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve(console.log(message));
        }, duration);
    });
};
myFunction("Hello", 3000)
    .then(function (res) { return myFunction("World", 2000); });
/*________________________________________________________________________________________*/
//two
var my3SecondsFunction = function () {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve("This process will take 3 seconds");
        }, 3000);
    });
};
var my4SecondsFunction = function () {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve("This process will take 4 seconds");
        }, 4000);
    });
};
Promise.all([my3SecondsFunction(), my4SecondsFunction()])
    .then(function (res) { return console.log(res[0], res[1]); });
