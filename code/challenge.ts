
/*________________________________________________________________________________________*/
                                        //one

//
const  myFunction = (message: string, duration: number) => {  
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
          resolve(console.log(message));
       }, duration); 
     });
}

myFunction("Hello", 3000)
    .then((res)=>myFunction("World", 2000));


/*________________________________________________________________________________________*/
                                    //two
const my3SecondsFunction = () => {
     return new Promise((resolve,reject)=>{
        setTimeout(() => {
          resolve(
                 "This process will take 3 seconds");
       }, 3000); 
     });  
}

const my4SecondsFunction = () => {
    return new Promise((resolve,reject)=>{
            setTimeout(() => {
                    resolve("This process will take 4 seconds")
             }, 4000); 
    }); 
}


Promise.all([my3SecondsFunction(), my4SecondsFunction()])
.then((res)=>console.log(res[0],res[1]));
